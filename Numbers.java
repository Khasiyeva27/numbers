import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        Random rand =new Random();
        int[] arr=new int[100];
        int count=0;

        while(true) {
            int randNum = rand.nextInt(101);
            System.out.println("Let the game begin!");
            System.out.println("Enter your name:");

            String name = sc.nextLine();
            while (true) {
                System.out.println("Enter your guess(0-100)");
                int num = sc.nextInt();
                sc.nextLine();

                arr[count]=num;
                count++;

                if (num < randNum) {
                    System.out.println("Your number is too small. Please, try again");
                } else if (num > randNum) {
                    System.out.println("Your number is too big. Please, try again");
                } else {
                    System.out.println("Congratulations, " + name + "!");
                    break;
                    }
                }
            if(count>0){
                System.out.println("Your numbers:");
                for(int i=0;i<count;i++){
                    int maxIndex=i;
                    for(int j=i;j<count;j++){
                        if(arr[j]>arr[maxIndex]){
                            maxIndex=j;
                        }
                    }
                    int temp=arr[maxIndex];
                    arr[maxIndex]=arr[i];
                    arr[i]=temp;
                }

                for(int i=0;i<count;i++){
                    System.out.print(arr[i]+" ");
                }

                System.out.println();
            }
            System.out.println("Countinue..?(yes/no)");
            String str=sc.nextLine().toLowerCase();

            if(!str.equals("yes")){
                System.out.println("Thank you");
                break;
            }
            }
        }
    }
